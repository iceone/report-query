# -*- coding: utf-8 -*-
FLOAT = 'float'
INT = 'int'
STR = 'str'
DATE = 'date'
TIME = 'time'
DATETIME = 'datetime'
INTERVAL = 'interval'
VIRTUAL_FIELD = 'virtual'

EQUALS = 1
NOT_EQUALS = 2
GREATER = 3
GREATER_EQUALS = 4
LOWER = 5
LOWER_EQUALS = 6
LIKE = 7
NOT_LIKE = 8
BETWEEN = 9

OPERATORS = {
    EQUALS: ('=', u'='),
    NOT_EQUALS: ('!=', u'≠'),
    GREATER: ('>', u'>'),
    GREATER_EQUALS: ('>=', u'≥'),
    LOWER: ('<', u'<'),
    LOWER_EQUALS: ('<=', u'≤'),
    LIKE: ('LIKE', u'Содержит'),
    NOT_LIKE: ('NOT LIKE', u'Не содержит')
}

PERIODICITIES = {
    '*/5 * * * *': u'Раз в 5 минут',
    '0 * * * *': u'Ежечасно',
    '0 23 * * *': u'Ежедневно (в 23:00)',
    '0 0 1 * *': u'Ежемесячно',
    '0 0 1 * 1': u'Еженедельно',
}
