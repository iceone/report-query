# -*- coding: utf-8 -*-
from __future__ import absolute_import
import string
import random
import datetime
import json
import cPickle
import logging

from flask import Blueprint, jsonify, request, Response
from flask.ext.login import current_user
from flask.views import MethodView
from werkzeug.local import LocalProxy

from flask import current_app

import report_query
from report_query.models import Report as GenericReport
from report_query.models import BadParameters
from report_query.models import PygmentsExtension
from report_query.signals import template_created, template_updated, template_deleted
from report_query.utils.json import JSONEncoder
from jinja2 import Environment, ChoiceLoader, FileSystemLoader

_report_template_model = None
ReportTemplate = LocalProxy(lambda: _report_template_model)

logger = logging.getLogger('report_query')

_db = None
db = LocalProxy(lambda: _db)

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in xrange(size))

class ReportQueryExtension(object):

    def __init__(self, app, reports, redis=None, template_dirs=None, permission_checker=None, user_model=None, model_bind_key=None):
        self.app = app
        self.db = app.extensions['sqlalchemy'].db
        # register db
        global _db
        _db = self.db

        self.report_template_model = get_report_template_model(self.db, bind_key=model_bind_key)

        # register model
        global _report_template_model
        _report_template_model = self.report_template_model

        # Setup template loaders
        _template_dirs = (template_dirs or []) + [report_query.models.DEFAULT_TEMPLATE_DIR]
        loader_array = map(FileSystemLoader, _template_dirs)
        # Setup environment
        self.jinja2_env = Environment(loader=ChoiceLoader(loader_array), extensions=[PygmentsExtension])
        self.redis = redis
        self.permission_checker = permission_checker
        self.user_model = user_model

        for report in reports:
            bp = create_blueprint(self, report, before_request=self.permission_checker('reports.%s' % report.__name__.lower()))
            self.app.register_blueprint(bp, url_prefix="/reports")


class Report(GenericReport):
    '''
    Flask-SQLAlchemy dialect for report_query
    '''
    connection_name = None

    @property
    def sql_log(self):
        return self._sql_log

    def sql(self, *args, **kwargs):
        sql = super(Report, self).sql(*args, **kwargs)
        self._sql_log.append({'time': '?', 'sql': sql})
        return sql

    def get_connection(self, name):
        return db.get_engine(db.get_app(), name).raw_connection()

    def get_connection_vendor(self):
        return db.engine.dialect.name

    def get_report_template(self, id):
        return  ReportTemplate.query.filter_by(id=id).first()


def get_report_template_model(db, bind_key=None):

    class ReportTemplate(db.Model):
        __bind_key__ = bind_key
        __tablename__ = 'report_template'
        __table_args__ = {'extend_existing': True}

        id = db.Column(u'id', db.BIGINT(), primary_key=True, nullable=False)
        name = db.Column(u'name', db.VARCHAR(length=63, convert_unicode=False, unicode_error=None, _warn_on_bytestring=False), nullable=False)
        params = db.Column(u'params', db.TEXT(length=None, convert_unicode=False, unicode_error=None, _warn_on_bytestring=False), nullable=False)
        periodicity = db.Column(u'periodicity', db.TEXT(length=None, convert_unicode=False, unicode_error=None, _warn_on_bytestring=False), nullable=False,
            server_default='')
        email = db.Column(u'email', db.TEXT(length=None, convert_unicode=False, unicode_error=None, _warn_on_bytestring=False), nullable=False,
            server_default='')
        public = db.Column(u'public', db.BOOLEAN(create_constraint=True, name=None), nullable=False)
        type = db.Column(u'type', db.VARCHAR(length=255, convert_unicode=False, unicode_error=None, _warn_on_bytestring=False))
        user_id = db.Column(u'auth_user_id', db.BIGINT(), info={"verbose_name": u"auth user"})
        created_datetime = db.Column(u'created_datetime', db.TIMESTAMP(timezone=True), nullable=False, default=datetime.datetime.now)
    return ReportTemplate


def create_get_config_view(ext, report_cls):

    class GetConfig(MethodView):

        def get(self):
            report = report_cls()
            report.user = current_user
            report_template_id = request.args.get('report_template_id')
            if report_template_id:
                report_template = ext.report_template_model.query.filter_by(id=report_template_id).first()
                if not report_template:
                    return jsonify({'message': 'Report template with id = %s does not exist' % report_template_id}), 404
                config = json.loads(report_template.params)
                report.load_config(config)
                report.report_template = report_template
            return jsonify(report.get_config(unbound=True))

    return GetConfig

def create_get_result_id_view(ext, report_cls):

    class GetResultId(MethodView):

        def post(self):

            config = request.get_json(force=True)
            report = report_cls(jinja2_env=ext.jinja2_env)
            report.user = current_user
            report.load_config(config)

            try:
                result = report.execute()
            except BadParameters as e:
                result = {'data': None}
                report.error = e.message or 'Unknown exception: %s' % e

            if not report.error:
                mime_type, rendered = report.render(data=result)
            else:
                data = {'success': False, 'message': report.error}
                # TODO: check permissions
                if report.show_sql:
                    data['sql'] = report.sql(format=True)
                return jsonify(data), 400

            filename = report.__class__.__name__.lower()
            timestamp = datetime.datetime.now().strftime('%Y_%m_%d__%H_%M_%S')
            filename = u'%s_%s' % (filename, timestamp)

            headers = {}

            if mime_type == 'application/vnd.ms-excel':
                headers['Content-Disposition'] = u'attachment; filename=%s.xls' % filename
            elif mime_type == 'application/csv':
                headers['Content-Disposition'] = u'attachment; filename=%s.csv' % filename
            response_params = dict(
                response=rendered, status=200, mimetype=mime_type, headers=headers
            )

            result_id = id_generator()
            key = 'report:result:%s' % result_id
            ext.redis.set(key, cPickle.dumps(response_params))
            ext.redis.expire(key, 5 * 60)

            return jsonify({'result_id': result_id})

    return GetResultId


def create_get_result_view(ext, report_cls):

    class GetResult(MethodView):

        def get(self, result_id):
            s = ext.redis.get('report:result:%s' % result_id)
            if not s:
                return jsonify({'message' :'Result is not found in cache'}), 400
            response_params = cPickle.loads(s)
            return Response(**response_params)

    return GetResult

def create_save_template_view(ext, report_cls):

    class SaveTemplate(MethodView):

        def post(self):
            from_client_config = request.get_json(force=True)
            save_as = from_client_config.pop('save_as', None)
            report = report_cls()
            report.user = current_user
            report.load_config(from_client_config)
            config = report.get_config()

            created = save_as or not report.report_template

            if save_as or not report.report_template:
                report.report_template = ext.report_template_model()
                report.report_template.user_id = current_user.id
                report.report_template.type = report_cls.__name__.lower()

            if not from_client_config.get('template_name'):
                return jsonify({'message': u'Empty template name'}), 400
            report.report_template.name = from_client_config['template_name']

            # TODO: permissions for public templates
            report.report_template.public = from_client_config.get('public', False)
            # TODO: convert text field -> json field
            report.report_template.params = json.dumps(config, cls=JSONEncoder)
            report.report_template.periodicity = report.periodicity or ''
            report.report_template.email = report.email or ''

            session = ext.db.session()
            session.add(report.report_template)
            session.commit()

            if created:
                template_created.send(report.report_template, config=config)
            else:
                template_updated.send(report.report_template, config=config)

            return jsonify(report.get_config())
    return SaveTemplate

def create_delete_template_view(ext, report_cls):

    class DeleteTemplate(MethodView):

        def delete(self, template_id):
            report_template = ext.report_template_model.query.get(template_id)
            session = ext.db.session()
            session.delete(report_template)
            session.commit()
            template_deleted.send(report_template, config=json.loads(report_template.params))
            return jsonify({'success': True})

    return DeleteTemplate

def create_blueprint(ext, report_cls, before_request=None):
    assert hasattr(report_cls, 'urlpattern')
    blueprintname = '{0}{1}'.format('report_query', report_cls.__name__.lower())
    _blueprint = Blueprint(blueprintname, __name__)

    # get config url
    config_view = create_get_config_view(ext, report_cls).as_view('get_config')
    _blueprint.add_url_rule(report_cls.urlpattern + '/config', methods=frozenset(('GET',)), view_func=config_view)
    # get result id url
    result_id_view = create_get_result_id_view(ext, report_cls).as_view('get_result_id')
    _blueprint.add_url_rule(report_cls.urlpattern + '/result_id', methods=frozenset(('POST',)), view_func=result_id_view)

    # get result url
    result_view = create_get_result_view(ext, report_cls).as_view('get_result')
    _blueprint.add_url_rule(report_cls.urlpattern + '/result/<string:result_id>', methods=frozenset(('GET',)), view_func=result_view)

    # delete template url
    delete_template_view = create_delete_template_view(ext, report_cls).as_view('delete_template')
    _blueprint.add_url_rule(report_cls.urlpattern + '/template/<int:template_id>', methods=frozenset(('DELETE',)), view_func=delete_template_view)

    # save template url
    save_template_view = create_save_template_view(ext, report_cls).as_view('save_template')
    _blueprint.add_url_rule(report_cls.urlpattern + '/template', methods=frozenset(('POST',)), view_func=save_template_view)

    if before_request is not None:
        _blueprint.before_request(before_request)

    return _blueprint
