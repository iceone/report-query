# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django.db import models, connections
from django.views.generic.base import View
from django.http import HttpResponse

from report_query.utils.api import CustomEncoder

from report_query.models import Report as GenericReport

class Report(GenericReport):
    '''
    Django dialect for report_query
    '''
    @property
    def sql_log(self):
        return self.connection.queries

    def get_connection(self, name):
        return connections[name]

    def get_connection_vendor(self):
        return self.connection.vendor

def DjangoJSONResponse(response, content_type=None):
    try:
        assert isinstance(response, dict)
        if 'success' not in response:
            response['success'] = True
    except Exception, e:
        # Come what may, we're returning JSON.
        if hasattr(e, 'message'):
            msg = e.message
        else:
            msg = 'Internal error'+': ' + str(e)
        response = {'success': False,
                    'message': msg}
    json = json.dumps(response, cls=CustomEncoder, ensure_ascii=False)

    code = [400, 200][response['success']]

    return HttpResponse(json, status=code, mimetype=(content_type or 'application/json; charset=utf-8'))


class DjangoJSONView(View):

    def __init__(self, *args, **kwargs):
        self._content_type = "application/json; charset=utf-8"
        super(DjangoJSONView, self).__init__(*args, **kwargs)

    def set_content_type(self, content_type):
        assert content_type in ('application/json; charset=utf-8', 'text/html; charset=utf-8')
        self._content_type = content_type

    @classmethod
    def permission_required(view, perm):
        def has_perm(func):
            def wrapper(*args, **kwargs):
                request = args[1]
                if perm is None or (request.user.is_authenticated() and request.user.has_perm(perm)):
                    return func(*args, **kwargs)
                else:
                    return HttpResponseForbidden()
            return wrapper
        return has_perm

    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        self.request = request
        self.args = args
        self.kwargs = kwargs
        result = handler(request, *args, **kwargs)
        if not isinstance(result, HttpResponse):
            return DjangoJSONResponse(result, content_type=self._content_type)
        return result


class ReportTemplate(models.Model):
    id = models.AutoField(u'Id', primary_key=True)
    name = models.CharField(u'Name', max_length=63)
    auth_user = models.ForeignKey('auth.User', db_column='auth_user_id', null=True, blank=True)
    public = models.BooleanField(u'Public', default=False)
    type = models.CharField(u'Тип', max_length=255, null=True, blank=True, help_text=u'Lowercase of report`s classname')
    params = models.TextField(u'Настройки')
    created_datetime = models.DateTimeField(u'Создан', auto_now_add=True)

    class Meta:
        app = 'report_template'
        db_table = u'report_template'


def get_django_view(cls):

    class ReportHandler(DjangoJSONView):

        @JSONView.permission_required(cls.permission)
        def get(self, request):
            report = cls()
            report.user = request.user
            report_template_id = request.GET.get('report_template_id')
            if report_template_id:
                report_template = ReportTemplate.objects.get(pk=report_template_id)
                config = json.loads(report_template.params)
                report.load_config(config)
                report.report_template = report_template
            return report.get_config(unbound=True)

        @JSONView.permission_required(cls.permission)
        def post(self, request):
            payload = request.read()
            config = json.loads(payload)
            report = cls()
            report.user = request.user
            report.load_config(config)

            try:
                result = report.execute()
            except BadParameters as e:
                result = {'data': None}
                report.error = e.message

            if not report.error:
                mime_type, rendered = report.render(data=result)
            else:
                data = {'success': False, 'message': report.error}
                if report.show_sql and request.user.has_perm('core.view_report_sql'):
                    data['sql'] = report.sql(format=True)
                return data

            response = HttpResponse(content=rendered, content_type=mime_type)
            filename = report.__class__.__name__.lower()
            timestamp = datetime.datetime.now().strftime('%Y_%m_%d__%H_%M_%S')

            filename = u'%s_%s' % (filename, timestamp)

            if mime_type == MIME_TYPES['excel']:
                response['Content-Disposition'] = u'attachment; filename=%s.xls' % filename
            elif mime_type == MIME_TYPES['csv']:
                response['Content-Disposition'] = u'attachment; filename=%s.csv' % filename
            return response

        @JSONView.permission_required(cls.permission)
        def put(self, request):
            payload = request.read()
            from_client_config = json.loads(payload)
            save_as = from_client_config.pop('save_as', None)
            report = cls()
            report.user = request.user
            report.load_config(from_client_config)
            config = report.get_config()

            if save_as or not report.report_template:
                report.report_template = ReportTemplate()
                report.report_template.user = None
                report.report_template.auth_user = request.user
                report.report_template.type = report.__class__.__name__.lower()

            if not from_client_config.get('template_name'):
                return {'success': False, 'message': u'Empty template name'}
            report.report_template.name = from_client_config['template_name']

            if request.user.has_perm('core.view_edit_public_templates'):
                report.report_template.public = from_client_config.get('public', False)
            report.report_template.params = json.dumps(config, cls=CustomEncoder, ensure_ascii=False)
            report.report_template.save()

            return report.get_config()

    return ReportHandler.as_view()
