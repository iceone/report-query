# -*- coding: utf-8 -*-
from blinker import Namespace

_signals = Namespace()
template_created = _signals.signal('template-created')
template_updated = _signals.signal('template-updated')
template_deleted = _signals.signal('template-deleted')
