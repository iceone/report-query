from __future__ import absolute_import
import csv
from .base import BaseRenderer
import cStringIO

def encode_dict(d):
    return dict((k, v.encode('utf-8') if isinstance(v, unicode) else v) for k, v in d.iteritems())

def render_csv(context):
    csv_file = cStringIO.StringIO()
    fieldnames = [f.name for f in context['report'].visible_fields]
    header_row = dict(zip(
        fieldnames,
        [f.column_name or f.label for f in context['report'].visible_fields],
    ))
    writer = csv.DictWriter(csv_file, fieldnames, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL, extrasaction='ignore')
    writer.writerow(encode_dict(header_row))
    for row in context['data']:
        writer.writerow(encode_dict(row))
    if context['report'].show_total:
        writer.writerow(encode_dict(context['total']))
    result = csv_file.getvalue()
    csv_file.close()
    return result


class Renderer(BaseRenderer):
    description = u'CSV'

    def render(self, context):
        if self._report.error:
            return ''
        return render_csv(context)
