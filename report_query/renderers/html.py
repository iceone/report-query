from .base import BaseRenderer

class Renderer(BaseRenderer):
    description = u'HTML'

    def render(self, context):
        if not self._report.error:
            context['data'], context['total'] = self.format_data(context['data'], context['total'])

        if self._report.template:
            template = self._report.jinja2_env.from_string(self._report.template)
        else:
            template = self._report.jinja2_env.get_template(self._report.template_path)
        return template.render(**context)
