from .pdf import Renderer as PdfRenderer
from .csv import Renderer as CsvRenderer
from .html import Renderer as HtmlRenderer
from .xls import Renderer as XLSRenderer

MIME_TYPES = {
    'html': 'text/html',
    'json': 'application/json',
    'pdf': 'application/pdf',
    'excel': 'application/vnd.ms-excel',
    'csv': 'text/csv'
}

class Renderers(dict):
    def get_renderer(self, mimetype):
        return self[mimetype][0]
    def get_file_extension(self, mimetype):
        return self[mimetype][1]

RENDERERS = Renderers({
    'text/html': (HtmlRenderer, 'html'),
    'application/vnd.ms-excel': (XLSRenderer, 'xls'),
    'application/csv': (CsvRenderer, 'csv'),
    'application/pdf': (PdfRenderer, 'pdf')
})

def register_renderer(mimetype, renderer, extension):
    RENDERERS[mimetype] = (renderer, extension)
