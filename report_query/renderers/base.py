
class BaseRenderer(object):
    description = ''

    def __init__(self, report):
        self._report = report

    def format_data(self, data, total):
        for row in data:
            for f in self._report.visible_fields:
                row[f.name] = f.format_value(row[f.name])
        if self._report.show_total:
            for item in total.subtotals:
                for f in self._report.visible_fields:
                    if f._report.calculate_total:
                        item['total'][f.name] = f.format_value(item['total'][f.name])
            for f in self._report.visible_fields:
                if f.calculate_total:
                    total[f.name] = f.format_value(total[f.name])
        return data, total

    def render(self):
        raise NotImplementedError()


