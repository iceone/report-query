# -*- coding: utf-8 -*-
import subprocess
from .html import Renderer as HTMLRenderer

try:
    subprocess.Popen(['wkhtmltopdf'],
                     stdout=subprocess.PIPE,
                     stdin=subprocess.PIPE,
                     stderr=subprocess.PIPE)
    subprocess.Popen(['xvfb-run'],
                     stdout=subprocess.PIPE,
                     stdin=subprocess.PIPE,
                     stderr=subprocess.PIPE)
except OSError:
    raise Exception("PDF rendering depends on wkhtmltopdf and xvfb packages. Please, install it with `apt-get install wkhtmltopdf xvfb`.")

DEFAULT_CSS = None

def register_css(css):
    global DEFAULT_CSS
    DEFAULT_CSS = css


class Renderer(HTMLRenderer):
    description = u'PDF'

    def render(self, context):
        html = super(Renderer, self).render(context)

        p = subprocess.Popen([
            'xvfb-run', '--auto-servernum',
            'wkhtmltopdf',
            '--encoding', 'utf-8',
            '-q',
            '-B', '5',
            '-L', '5',
            '-R', '5',
            '-T', '5',
            '-', '-'],
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE)

        stdout, stderr = p.communicate(html.encode('utf-8'))

        return stdout
