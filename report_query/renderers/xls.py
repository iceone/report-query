# -*- coding: utf-8 -*-
from .base import BaseRenderer
from pyExcelerator import Workbook, XFStyle, ExcelFormula, Borders, Pattern
import cStringIO
import datetime
from report_query.constants import INT, FLOAT, TIME, INTERVAL, DATE, DATETIME

def get_excel_value_format(type, format=None):
    if type == 'str':
        return 'general'
    elif type == INT:
        return 'general'
    elif type == FLOAT:
        return '0.00'
    elif type in (TIME, INTERVAL):
        return '[hh]:mm:ss'
    elif type == DATE:
        return 'DD.MM.YYYY'
    elif type == DATETIME:
        if format == '%H:%M:%S':
            return 'hh:mm:ss'
        elif format == '%H:%M':
            return 'hh:mm'
        elif format == '%M:%S':
            return 'mm:ss'
        return 'DD.MM.YYYY hh:mm:ss'
    return 'general'

def convert_value_to_excel(value):
    if value is None:
        return ''
    # datetime is instance of date
    if type(value) is datetime.date:
        return datetime.datetime.fromordinal(value.toordinal()) if value else ''
    elif type(value) is datetime.datetime:
        return value.replace(tzinfo=None) if value else ''
    elif hasattr(value, '__class__') and value.__class__.__name__ == 'Decimal':
        return float(value)
    else:
        return value


class Renderer(BaseRenderer):
    description = u'Excel (97-2003)'

    def render(self, context):
        return render_excel(context)

def render_excel(context):
    xls_file = cStringIO.StringIO()
    wb = Workbook()
    ws0 = wb.add_sheet('0')

    header_style = XFStyle()
    header_style.font.name = 'Arial'
    header_style.font.bold = True
    header_style.borders.left = Borders.THIN
    header_style.borders.right = Borders.THIN
    header_style.borders.top = Borders.THIN
    header_style.borders.bottom = Borders.THIN
    header_style.pattern.pattern = Pattern.SOLID_PATTERN
    header_style.pattern.set_pattern_fore_colour('silver')

    fieldnames = [f.name for f in context['report'].visible_fields]
    header_row = dict(zip(
        fieldnames,
        [f.column_name or f.label for f in context['report'].visible_fields],
    ))

    x_offset = 0
    if context['report'].enumerate_rows:
        x_offset += 1

    for num, field in enumerate(context['report'].visible_fields):
        row_counter = 0
        name = field.name
        x_pos = num + x_offset

        data_style = XFStyle()
        data_style.font.name = 'Arial'
        data_style.num_format_str = get_excel_value_format(field.type, field.format)
        data_style.borders.left = Borders.THIN
        data_style.borders.right = Borders.THIN
        data_style.borders.top = Borders.THIN
        data_style.borders.bottom = Borders.THIN

        total_style = XFStyle()
        total_style.num_format_str = get_excel_value_format(field.type, field.format)
        total_style.font.name = 'Arial'
        total_style.font.bold = True
        total_style.borders.left = Borders.THIN
        total_style.borders.right = Borders.THIN
        total_style.borders.top = Borders.THIN
        total_style.borders.bottom = Borders.THIN
        total_style.pattern.pattern = Pattern.SOLID_PATTERN
        total_style.pattern.set_pattern_fore_colour('silver')

        # header
        if context['report'].title:
            ws0.write(row_counter, 0, context['report'].title)
            row_counter += 1
        for fltr in context['report'].filters_list:
            if fltr.show_values and fltr.value:
                text = u'%s: %s' % (fltr.label, u', '.join(fltr.verbose_values))
                ws0.write(row_counter, 0, text)
                row_counter += 1

        # выводим номер строки
        if context['report'].enumerate_rows and num == 0:
            ws0.write(row_counter, 0, u'№ п/п', header_style)
            ws0.col(0).width = 1500  # 1.5cm

        ws0.write(row_counter, x_pos, header_row[name], header_style)
        row_counter += 1

        # data
        def get_col_name(idx):
            if idx < 1:
                raise ValueError("Index is too small")
            result = ""
            while True:
                if idx > 26:
                    idx, r = divmod(idx - 1, 26)
                    result = chr(r + ord('A')) + result
                else:
                    return chr(idx + ord('A') - 1) + result

        def translate(y, x, fixed_col=False, fixed_row=False):
            col_fixer = '$' if fixed_col else ''
            row_fixer = '$' if fixed_row else ''
            return u'%s%s%s%d' % (col_fixer, get_col_name(x+1), row_fixer, y+1)

        if context['report'].show_total and context['total'].subtotals:
            subtotal_cells = []
            for subtotal in context['total'].subtotals:
                rows_count = len(subtotal['group'])
                # для запоминания области суммирования
                first_cell, last_cell = None, None

                for row_idx, row in enumerate(subtotal['group']):
                    is_first = row_idx == 0
                    is_last = row_idx == rows_count - 1

                    # выводим номер строки
                    if num == 0:
                        ws0.write(row_counter, 0, row._number, data_style)

                    value = convert_value_to_excel(row[name])
                    ws0.write(row_counter, x_pos, value, data_style)
                    # запоминаем границы области данных
                    if is_first:
                        first_cell = (row_counter, x_pos)  # (y, x)
                    if is_last:
                        last_cell = (row_counter, x_pos)  # (y, x)
                    row_counter += 1

                # subtotal
                total_value = ''
                if context['report'].enumerate_rows and num == 0:
                    ws0.write(row_counter, 0, u'', total_style)

                if field.calculate_total:
                    total_value = ExcelFormula.Formula('SUM(%s:%s)' % (translate(*first_cell, fixed_col=True), translate(*last_cell, fixed_col=True)))
                    subtotal_cells.append((row_counter, x_pos))
                ws0.write(row_counter, x_pos, total_value, total_style)
                row_counter += 1
            # total
            if field.calculate_total:
                total_value = ExcelFormula.Formula('+'.join(map(lambda x: translate(*x, fixed_col=True), subtotal_cells)))

            if context['report'].enumerate_rows and num == 0:
                ws0.write(row_counter, 0, u'Итого', total_style)

            ws0.write(
                row_counter, x_pos,
                total_value,
                total_style
            )
        else:
            rows_count = len(context['data'])
            # для запоминания области суммирования
            first_cell, last_cell = None, None

            for row_idx, row in enumerate(context['data']):
                is_first = row_idx == 0
                is_last = row_idx == rows_count - 1
                # выводим номер строки
                if num == 0:
                    ws0.write(row_counter, 0, row._number, data_style)

                value = convert_value_to_excel(row[name])
                ws0.write(row_counter, x_pos, value, data_style)
                # запоминаем границы области данных
                if is_first:
                    first_cell = (row_counter, x_pos)  # (y, x)
                if is_last:
                    last_cell = (row_counter, x_pos)  # (y, x)
                row_counter += 1
            # total
            if context['report'].show_total:
                total_value = ''
                if context['report'].enumerate_rows and num == 0:
                    ws0.write(row_counter, 0, u'Итого', total_style)
                if field.calculate_total:
                    total_value = ExcelFormula.Formula('SUM(%s:%s)' % (translate(*first_cell, fixed_col=True), translate(*last_cell, fixed_col=True)))
                ws0.write(row_counter, x_pos, total_value, total_style)

    wb.save(xls_file)
    result = xls_file.getvalue()
    xls_file.close()
    return result
