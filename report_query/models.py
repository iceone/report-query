# -*- coding: utf-8 -*-
import os
import os.path
import copy
import datetime
import operator
from operator import attrgetter
from psycopg2.extensions import adapt

from itertools import groupby
import functools

import dateutil

from logging import getLogger

from jinja2 import Environment, ChoiceLoader, FileSystemLoader
from jinja2 import nodes
from jinja2.ext import Extension

from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import guess_lexer, get_lexer_by_name

from utils.db import sql_format
from utils.timezone import to_local_tz, get_tz
from utils.datetime import parse_timedelta, parse_date, parse_datetime

from .constants import FLOAT, INT, STR, DATE, DATETIME, TIME, INTERVAL, VIRTUAL_FIELD
from .constants import EQUALS, LIKE, NOT_LIKE, BETWEEN
from .constants import OPERATORS, PERIODICITIES

from renderers import RENDERERS

DEFAULT_TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')

logger = getLogger('report_query')


class PygmentsExtension(Extension):
    """
    A Pygments extension for use with the Jinja2 template language.

    Setup:

    import PygmentsExtension
    from jinja2 import Environment

    jinja2_env = Environment(extensions=[PygmentsExtension])

    Usage:

    {% code 'javascript' %}
    function foo() { console.log('bar'); }
    {% endcode %}
    """
    tags = set(['code'])

    def __init__(self, environment):
        super(PygmentsExtension, self).__init__(environment)

        # add the defaults to the environment
        environment.extend(
            pygments=self
        )

    def parse(self, parser):
        lineno = parser.stream.next().lineno

        args = []
        lang_type = parser.parse_expression()

        if lang_type is not None:
            args.append(lang_type)

        body = parser.parse_statements(['name:endcode'], drop_needle=True)

        return nodes.CallBlock(self.call_method('_pygmentize', args),
                                [], [], body).set_lineno(lineno)

    def _pygmentize(self, lang_type, caller):
        lexer = None
        #formatter = HtmlFormatter(linenos='table')
        formatter = HtmlFormatter()
        content = caller()

        if lang_type is None:
            lexer = guess_lexer(content)
        else:
            lexer = get_lexer_by_name(lang_type)

        return highlight(content, lexer, formatter)

working_dir = os.path.dirname(os.path.realpath(__file__))

# Setup template loaders
loader_array = [
    FileSystemLoader(os.path.join(working_dir, 'templates')),
    FileSystemLoader(os.path.join(working_dir, '../reports/templates'))
]

# Setup environment
local_jinja2_env = Environment(loader=ChoiceLoader(loader_array), extensions=[PygmentsExtension])


def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

def dict_cursor(cursor):
    desc = cursor.description
    return [Row(zip([col[0] for col in desc], row)) for row in cursor.fetchall()]


def NOOP_FUNC(*args, **kwargs):
    return


def get_series_data(_from, to):
    result = []
    for x in xrange(_from, to+1):
        result.append({'id': x, 'name': unicode(x)})
    return result


def get_data_from_queryset(qs, id='id', name='name', now=False):
    def make_data(qs):
        result = []
        for item in qs:
            result.append({
                'id': getattr(item, id),
                'name': getattr(item, name)
            })
        return result
    if now:
        return make_data(qs)
    return functools.partial(make_data, qs)


def to_unicode(s, charset='utf-8'):
    return s if isinstance(s, unicode) else s.decode(charset)


def format_timedelta(value):
    value = parse_timedelta(value)
    seconds = value.total_seconds()
    if seconds > 86400:
        hours, remainder = divmod(abs(seconds), 3600)
        minutes, seconds = divmod(remainder, 60)
        return u'%s%d:%02d:%02d' % (('', '-')[seconds < 0], hours, minutes, seconds)
    else:
        return u'%s%s' % (('', '-')[seconds < 0], value.strftime('%H:%M:%S'))


def format_value(value=None, typ=None, format=None):
    if value is None:
        return ''
    if format is None:
        return value
    if isinstance(value, basestring) and typ != STR:
        value = to_python(value, typ)
    if typ == FLOAT:
        return (format % value).replace('.', ',')
    elif typ in (DATETIME, DATE):
        return value.strftime(format)
    elif typ in (TIME, INTERVAL):
        value = parse_timedelta(value)
        seconds = value.total_seconds()
        hours, remainder = divmod(abs(seconds), 3600)
        minutes, seconds = divmod(remainder, 60)
        return u'%s%d:%02d:%02d' % (('', '-')[seconds < 0], hours, minutes, seconds)
    else:
        try:
            return format % value
        except:
            raise Exception(u'Error in format value: format: %s, value: %s' % (format, value))


def prepare_value_for_db(value, type):
    if type == STR:
        if isinstance(value, unicode):
            result = adapt(value.encode('utf-8'))
        else:
            result = adapt(value)
    elif type == INT:
        result = adapt(int(value))
    elif type == FLOAT:
        result = adapt(float(value))
    elif type == TIME:
        result = adapt(value.strftime('%H:%M:%S'))
    elif type == INTERVAL:
        result = adapt(format_timedelta(value))
    elif type == DATE:
        result = adapt(value.strftime('%Y-%m-%d'))
    elif type == DATETIME:
        value = to_local_tz(value)
        result = adapt(value.strftime('%Y-%m-%d %H:%M:%S%z'))
    else:
        result = adapt(value)
    return to_unicode(result.getquoted())


def to_python(value, type):
    if isinstance(value, (tuple, list)):
        return [to_python(x, type) for x in value]
    if type == STR:
        return unicode(value)
    elif type == INT:
        return int(value)
    elif type == FLOAT:
        return float(value)
    elif type in (TIME, INTERVAL):
        return parse_timedelta(value)
    elif type == DATE:
        return parse_date(value)
    elif type == DATETIME:
        if isinstance(value, basestring):
            value = dateutil.parser.parse(value)
        return to_local_tz(value)


def to_json(value, type):
    if isinstance(value, (tuple, list)):
        return [to_json(x, type) for x in value]
    if type == STR:
        return unicode(value)
    elif type == INT:
        return int(value)
    elif type == FLOAT:
        return float(value)
    elif type in (TIME, INTERVAL):
        return format_timedelta(value)
    elif type == DATE:
        return parse_date(value).strftime('%Y-%m-%d')
    elif type == DATETIME:
        return parse_datetime(value).strftime('%Y-%m-%d %H:%M:%S')


class BadParameters(Exception):
    pass


class AvgTotal(object):

    def __init__(self):
        self.agg = []

    def __call__(self, prev_val, cur_val):
        self.agg.append(cur_val)
        return sum(self.agg)/float(len(self.agg))


class UnicodeOrCallable(object):
    _value = None
    _args = None

    def __init__(self, *args):
        self._value = args[0]
        if len(args) > 1:
            self._args = args[1:]

    def get_value(self):
        return to_unicode(self._value if not callable(self._value) else self._value(*self._args))


class BaseObject(object):
    creation_counter = 0
    name = None
    reference_name = None
    _args = None
    _kwargs = None

    class __metaclass__(type):

        def __init__(cls, name, bases, attrs):
            cls._fields_set = set(attrs.keys())

    def __init__(self, *args, **kwargs):
        assert self._args is not None and self._kwargs is not None

        self.position = int(BaseObject.creation_counter)
        BaseObject.creation_counter += 1

    def __setattr__(self, name, value):
        # register attribute even it exists
        self._fields_set.add(name)
        super(BaseObject,self).__setattr__(name, value)

    def register_args(self, args, kwargs):
        if self._args is None:
            self._args = args
        if self._kwargs is None:
            self._kwargs = kwargs

    def clone(self):
        obj = type(self)(*self._args, **self._kwargs)
        for k in self._fields_set:
            try:
                val = getattr(self, k, None)
            except:
                continue
            if callable(val):
                setattr(obj, k, val)
                continue
            try:
                setattr(obj, k, copy.deepcopy(val))
            except:
                continue
        return obj


class Field(BaseObject):
    name = None
    alias = None
    type = None
    format = None
    hidden = False
    show = False
    order = None
    order_by = None
    column_name = None
    using_table = None
    _report = None
    sql = None

    def __init__(self, *args, **kwargs):
        self.register_args(list(args), dict(kwargs))
        # args section
        label = args[0]
        # kwargs section
        alias = kwargs.pop('alias', None)
        sql = kwargs.pop('sql', None)
        type = kwargs.pop('type', None)
        order_by = kwargs.pop('order_by', None)
        format = kwargs.pop('format', None)
        agg = kwargs.pop('agg', 'max')
        show = kwargs.pop('show', None)
        hidden = kwargs.pop('hidden', False)
        total = kwargs.pop('total', False)
        column_name = kwargs.pop('column_name', None)
        using = kwargs.pop('using', None)
        self.total_func = kwargs.pop('total_func', operator.add)

        self.label = to_unicode(label)
        if sql:
            self.sql = to_unicode(sql) if isinstance(sql, basestring) else sql
        if type:
            self.type = type
        if format:
            self.format = to_unicode(format)
        if alias:
            self.alias = to_unicode(alias)
        if show:
            self.show = True

        self.aggregate = agg
        self.hidden = hidden
        self.calculate_total = total
        self.using_table = using
        # sql string for ordering field
        if order_by:
            self.order_by = to_unicode(order_by)
        if column_name:
            self.column_name = to_unicode(column_name)
        super(Field, self).__init__()

    def get_sql(self, aggregate=None, vendor='mysql', alias=True):
        if not self.sql:
            return
        _sql = self.sql if not callable(self.sql) else self.sql()
        aggregate = aggregate or self.name in self._report.fields_to_aggregate
        if aggregate:
            if self.aggregate == 'none':
                format = '%s'
            elif self.aggregate == 'min':
                format = 'MIN(%s)'
            elif self.aggregate == 'max':
                format = 'MAX(%s)'
            elif self.aggregate == 'avg':
                format = 'AVG(%s)'
            elif self.aggregate == 'sum':
                if self.type in (TIME, INTERVAL) and vendor == 'mysql':
                    format = 'SEC_TO_TIME(SUM(TIME_TO_SEC(%s)))'
                else:
                    format = 'SUM(%s)'
            elif self.aggregate == 'count':
                format = 'COUNT(%s)'
            elif self.aggregate == 'count:distinct':
                format = 'COUNT(DISTINCT %s)'
            elif 'string_agg' in self.aggregate:
                # string_agg:distinct:delimiter:order_by
                bits = self.aggregate.split(':')
                try:
                    distinct = bits[1]
                except IndexError:
                    distinct = ''
                try:
                    delimiter = bits[2] or ', '
                except IndexError:
                    delimiter = ', '
                try:
                    order_by = bits[3] and 'order by {0}'.format(bits[3])
                except IndexError:
                    order_by = ''
                format = "string_agg({0} %s, '{1}' {2})".format(distinct, delimiter, order_by)
        else:
            format = '%s'
        if alias:
            result = '%s AS "%s"' % (format % _sql, self.alias)
        else:
            result = format % _sql
        return result

    def format_value(self, value):
        return format_value(value=value, typ=self.type, format=self.format)


class IntegerField(Field):
    type = INT
    format = '%d'


class FloatField(Field):
    type = FLOAT
    format = '%.2f'


class CharField(Field):
    type = STR
    format = '%s'


class DateTimeField(Field):
    type = DATETIME
    format = '%d.%m.%Y'


class DateField(Field):
    type = DATE
    format = '%d.%m.%Y'


class TimeField(Field):
    type = TIME
    format = '%H:%M:%S'


class IntervalField(Field):
    type = INTERVAL
    format = '%H:%M:%S'


class VirtualField(Field):
    type = VIRTUAL_FIELD
    func = None
    depends = None

    def __init__(self, *args, **kwargs):
        self.register_args(list(args), dict(kwargs))
        func = kwargs.pop('func')
        depends = kwargs.pop('depends', [])
        assert func is not None and callable(func)
        if not kwargs.get('type'):
            raise Exception(u'VirtualField needs type to be set explicitly')
        assert isinstance(depends, (tuple, list))
        self.func = func
        self.depends = depends
        super(VirtualField, self).__init__(*args, **kwargs)


class BooleanField(Field):
    type = INT
    values = {
        0: u'Нет',
        False: u'Нет',
        1: u'Да',
        True: u'Да',
        None: u''
    }

    def __init__(self, *args, **kwargs):
        self.register_args(list(args), dict(kwargs))
        values = kwargs.pop('values', self.values)
        assert isinstance(values, dict)
        self.values = values
        super(BooleanField, self).__init__(*args, **kwargs)

    def format_value(self, value):
        return self.values[value]


# Filters

class Filter(BaseObject):
    _report = None
    field = None
    condition_type = None
    sql = None
    _value_type = None
    operator = EQUALS
    show_values = False
    widget_name = None
    widget_initial = {}
    widget_data = None
    using_table = None
    sql_template = None
    default = None
    only_if = None
    _used = None
    hidden = False
    advanced = False
    save = True
    is_free = False
    title_tpl = u'{label} {operator} {value}'

    def __init__(self, *args, **kwargs):
        self.register_args(list(args), dict(kwargs))
        # args section
        label = args[0]
        # kwargs section
        sql = kwargs.pop('sql', None)
        condition_type = kwargs.pop('condition_type', None)
        field = kwargs.pop('field', None)
        value_type = kwargs.pop('value_type', None)
        show_values = kwargs.pop('show_values', None)
        widget = kwargs.pop('widget', None)
        widget_initial = kwargs.pop('widget_initial', None)
        widget_data = kwargs.pop('widget_data', None)
        required = kwargs.pop('required', False)
        advanced = kwargs.pop('advanced', False)
        using = kwargs.pop('using', None)
        sql_template = kwargs.pop('sql_template', None)
        default = kwargs.pop('default', None)
        save = kwargs.pop('save', None)
        only_if = kwargs.pop('only_if', None)
        operator = kwargs.pop('operator', None)

        try:
            self.is_free = kwargs.pop('is_free')
        except KeyError:
            pass

        try:
            self.title_tpl = kwargs.pop('title_tpl')
        except KeyError:
            pass

        self.label = to_unicode(label)

        if field:
            self.field = field

        self.required = required
        self.advanced = advanced

        if condition_type:
            self.condition_type = condition_type
        if sql:
            self.sql = to_unicode(sql) if isinstance(sql, basestring) else sql
        if value_type:
            self._value_type = value_type
        if show_values:
            self.show_values = show_values
        if widget:
            self.widget_name = widget
        if widget_initial:
            self.widget_initial = widget_initial
        if widget_data:
            self.widget_data = widget_data
        if only_if:
            self.only_if = only_if
        self.using_table = using
        if sql_template:
            self.sql_template = to_unicode(sql_template)

        self.verbose_values = []
        self.value = []

        if save is not None:
            self.save = save

        if default is not None:
            self.default = default
            self.value = default() if callable(default) else default

        if operator is not None:
            self.operator = operator

        super(Filter, self).__init__()

    @property
    def operator_title(self):
        try:
            return OPERATORS[self.operator][1]
        except (KeyError, IndexError):
            return ''

    def get_title(self, title_tpl=None, **kwargs):
        _title_tpl = title_tpl or self.title_tpl
        return _title_tpl.format(**{
            'label': self.label,
            'operator': self.operator_title,
            'value': u', '.join(self.verbose_values)
        })

    @property
    def title(self):
        return self.get_title()

    @property
    def value_type(self):
        if self._value_type:
            return self._value_type
        if self.field and self._report:
            try:
                field = self._report.fields[self.field]
            except KeyError:
                return None
            return field.type

    def get_what(self):
        what = None
        if self.condition_type == 'where':
            # Если SQL определение непустое - берем его
            if self.sql:
                _sql = self.sql if not callable(self.sql) else self.sql()
                what = u'(%s)' % _sql
            # Иначе - ищем среди полей (вариант для custom-фильтров)
            else:
                if self.field and self.field in self._report.fields:
                    field = self._report.fields[self.field]
                elif self.name and self.name in self._report.fields:
                    field = self._report.fields[self.name]
                else:
                    return ''
                what = u'(%s)' % field.sql() if callable(field.sql) else field.sql

        elif self.condition_type == 'having':
            field = self._report.fields[self.field]
            what = field.get_sql(alias=False, vendor=self._report.vendor)
        return what

    def get_sql(self):
        # Если ни один параметр не указан - ничего не делаем
        if not self.value:
            return

        sql_template = self.sql_template
        if not sql_template:
            sql_template = self.build_sql_template()
        value_dict = self.get_value_dict()
        if not value_dict['what']:
            return ''
        return sql_template % value_dict

    def build_sql_template(self):
        # Массив значений
        if len(self.value) > 1:
            if self.operator == EQUALS:
                template = u'IN (%s)' % ', '.join(map(lambda x: u'%%(%d)s' % x, xrange(len(self.value))))
            elif self.operator == BETWEEN:
                template = u'BETWEEN %(0)s AND %(1)s'
        # Одиночные значения
        else:
            operator = OPERATORS[self.operator][0]
            if self.operator in (LIKE, NOT_LIKE):
                escaped = prepare_value_for_db(to_unicode(self.value[0]), STR)[1:-1]
                template = u'%s %s' % (operator, prepare_value_for_db(u"%%%%%s%%%%" % escaped, STR))
            else:
                template = u'%s %%(0)s' % operator
        return u'%%(what)s %s' % template

    def get_value_dict(self):
        '''
        Returns indexed dict
        '''
        what = self.get_what()
        if not self.value:
            return {'what': what}
        result = dict(zip(
            map(str, range(len(self.value))),
            map(lambda x: prepare_value_for_db(x, self.value_type), self.value)
        ))
        result['what'] = what
        return result

    def load_config_value(self, value, level=0):
        if level == 0:
            self.value = []
        if isinstance(value, dict):
            _value = to_python(value['value'], self.value_type)
            if isinstance(_value, (tuple, list)):
                self.value.extend(_value)
            else:
                self.value.append(_value)
            self.verbose_values.append(value['verbose'])
        elif isinstance(value, (tuple, list)):
            for vv in value:
                self.load_config_value(vv, level=level+1)
        elif isinstance(value, basestring) and not value:
            # ничего не делаем
            return
        elif value is not None:
            self.value.append(to_python(value, self.value_type))
            self.verbose_values.append(unicode(value))

    def get_config_value(self):
        # TODO: verbose handling
        if not self.value:
            return None
        value = to_json(self.value, self.value_type)
        if len(value) == 1:
            return value[0]
        return value

    def is_used(self):
        return bool(self.value)


class PlainFilter(Filter):
    pass


class SimpleCondition(BaseObject):
    hidden = True
    using_table = None
    only_if = None

    def __init__(self, sql):
        self.register_args([sql], {})
        self._sql = sql
        super(self.__class__, self).__init__()

    def get_sql(self):
        return self._sql

    def is_used(self):
        return True


class Having(Filter):
    condition_type = 'having'


class Where(Filter):
    condition_type = 'where'


class LikeFilter(Where):
    operator = LIKE


class RangeFilter(Where):
    operator = BETWEEN

    def get_config_value(self):
        if not self.value:
            return None
        value = {
            'value': to_json(self.value, self.value_type)
        }
        if self.verbose_values:
            value['verbose'] = self.verbose_values
        return value


class IntegerRangeFilter(Where):
    _value_type = INT
    widget_name = 'rangefield'
    widget_initial = {
        'valueFieldInit': {'minValue': 0, 'maxValue': 23},
        'valueField': 'numberfield',
        'width': 420
    }


class DateRangeFilter(RangeFilter):
    _value_type = DATE
    widget_name = 'rangefield'
    widget_initial = {
        'valueFieldInit': {'format': 'd.m.Y'},
        'valueField': 'datefield',
        'width': 420
    }
    save = False


class DateTimeRangeFilter(RangeFilter):
    _value_type = DATETIME
    widget_name = 'rangefield'
    widget_initial = {
        'valueField': 'datetimefield',
        'valueFieldInit': {'autoNow': True},
        'width': 600,
        'valueFieldWidth': 200
    }
    save = False


class TimeRangeFilter(RangeFilter):
    _value_type = TIME
    widget_name = 'rangefield'
    widget_initial = {
        'valueFieldInit': {'format': 'H:i', 'altFormats': 'H:i:s|H:i', 'increment': 60},
        'valueField': 'timefield',
        'width': 420
    }

    @property
    def sql_template(self):
        if self.value[0] <= self.value[1]:
            return u'%(what)s BETWEEN %(0)s AND %(1)s'
        else:
            # переход через сутки
            return u"(%(what)s BETWEEN %(0)s AND '23:59:59' OR %(what)s BETWEEN '00:00:00' AND %(1)s)"


class Placeholder(BaseObject):
    name = None
    label = None
    value = None

    def __init__(self, label=None, value=None):
        self.register_args([], {'label': label, 'value': value})
        if label is not None:
            self.label = label
        if value is not None:
            self.value = value
        super(Placeholder, self).__init__()

    def __str__(self):
        return self.value

    def __unicode__(self):
        return self.value


class Table(BaseObject):
    name = None
    alias = None
    join = None
    on_sql = None
    only_if = None
    sql = None
    virtual = False
    optional = False
    _report = None

    def __init__(self, *args, **kwargs):
        self.register_args(list(args), dict(kwargs))
        # kwargs section
        name = kwargs.pop('name', None)
        alias = kwargs.pop('alias', None)
        main = kwargs.pop('main', None)
        join = kwargs.pop('join', None)
        on = kwargs.pop('on', None)
        only_if = kwargs.pop('only_if', None)
        sql = kwargs.pop('sql', None)
        optional = kwargs.pop('optional', False)

        self.name = name
        self.alias = alias or name
        self.optional = optional

        if main:
            self.main = main
        if join:
            self.join = join
        if on:
            self.on_sql = on
        if only_if is not None:
            self.only_if = only_if
        if self.join and self.join != 'CROSS JOIN':
            assert self.on_sql
        if sql:
            self.virtual = True
            self.sql = sql
        super(Table, self).__init__()

    def get_sql(self):
        alias = self.alias or self.name

        if self.only_if:
            if not self.only_if(self._report):
                # this means join will not perform
                return None

        if self.virtual:
            table_or_sql = u'(%s\n)' % UnicodeOrCallable(self.sql, self._report).get_value()
        else:
            table_or_sql = self.name

        if self.main:
            return u'%s AS "%s"' % (table_or_sql, alias)
        else:
            if self.join == 'CROSS JOIN':
                return u'%s %s AS "%s"' % (self.join, table_or_sql, alias)
            else:
                on_sql = UnicodeOrCallable(self.on_sql, self._report).get_value()
                return u'%s %s AS "%s" ON %s' % (self.join, table_or_sql, alias, on_sql)


class FromTable(Table):
    main = True


class JoinTable(Table):
    main = False
    join = 'JOIN'


class LeftJoinTable(Table):
    main = False
    join = 'LEFT JOIN'


class RightJoinTable(Table):
    main = False
    join = 'RIGHT JOIN'


class CrossJoinTable(Table):
    main = False
    join = 'CROSS JOIN'


class ObjectsGroup(object):
    class __metaclass__(type):

        def __init__(cls, name, bases, attrs):
            cls._fields = {}
            cls._fields_list = []
            for key, value in attrs.iteritems():
                if isinstance(value, BaseObject):
                    value.reference_name = key
                    if value.name is None:
                        value.name = key
                    cls._fields[key] = value
                    cls._fields_list.append(value)

            cls._fields_list = sorted(cls._fields_list, key=attrgetter('position'))

    @classmethod
    def get_fields(cls):
        fields_list = []
        fields_dict = {}
        for f in cls._fields_list:
            cloned = f.clone()
            fields_dict[f.name] = cloned
            fields_list.append(cloned)
        return fields_list, fields_dict


class Row(dict):
    _all_fields = None
    _real_fields = None
    _virtual_fields = None

    def __getattr__(self, key):
        return self.__getitem__(key)

    def __setattr__(self, key, value):
        return self.__setitem__(key, value)


class Meta(object):
    def __init__(self):
        self.group_by = []
        self.default_group_by = []


class Report(object):
    # report settings
    name = u''
    title = u''
    show_total = False
    enumerate_rows = False
    show_sql = False
    email = ''
    mime_type = 'text/html'
    template = None
    report_template = None
    error = False
    user = None
    template_path = 'default_report.html'
    permission = None
    periodicity = None
    connection_name = 'default'
    sql_template = None
    show_fields = True
    show_group_by = True
    show_order_by = True
    urlpattern = None
    parent = None
    customizable = True
    _cursor = None
    distinct = False
    with_queries = None
    mime_types = RENDERERS.keys()

    def __init__(self, jinja2_env=None, connection=None):
        self.config = {}

        # fields
        if hasattr(self, 'Fields'):
            self.fields_list, self.fields = self.Fields.get_fields()
        else:
            self.fields_list, self.fields = [], {}

        for name, f in self.fields.iteritems():
            f._report = self
            f.name = name
            if not f.alias:
                f.alias = name
        self.real_fields = filter(lambda x: not isinstance(x, VirtualField), self.fields_list)
        self.virtual_fields = filter(lambda x: isinstance(x, VirtualField), self.fields_list)
        self.visible_fields = []

        self.fields_to_aggregate = []

        # filters
        if hasattr(self, 'Filters'):
            self.filters_list, self.filters = self.Filters.get_fields()
        else:
            self.filters_list, self.filters = [], {}

        for name, f in self.filters.iteritems():
            f._report = self

        # tables
        if hasattr(self, 'Tables'):
            self.tables_list, self.tables = self.Tables.get_fields()
        else:
            self.tables_list, self.tables = [], {}

        for name, t in self.tables.iteritems():
            t._report = self

        self.from_list = filter(lambda x: x.main is True, self.tables_list)
        self.join_list = filter(lambda x: x.main is False, self.tables_list)

        self.where_list = []
        self.having_list = []
        self.group_by_list = []
        self.order_by_list = []

        self.meta = getattr(self, 'Meta', Meta())
        self.default_group_by = list(getattr(self.meta, 'default_group_by', []))
        self.group_by = list(getattr(self.meta, 'group_by', []))

        # placeholders
        placeholders = getattr(self, 'Placeholders', None)
        if placeholders:
            self.placeholders_list, self.placeholders = placeholders.get_fields()
        else:
            self.placeholders_list, self.placeholders = [], {}

        # путь к шаблону
        template_path = getattr(self.meta, 'template', None)
        if template_path:
            self.template_path = template_path

        if connection:
            self.connection = connection
        else:
            # if self.connection_name is None:
            #     raise Exception('Report class does not provide connection name')
            self.connection = self.get_connection(self.connection_name)
        self.vendor = self.get_connection_vendor()

        self.jinja2_env = jinja2_env or local_jinja2_env
        # for using in preprocess
        self.extra_context = {}
        self._sql_log = []

    def add_template_filter(self, filter_name, filter_func):
        if filter_name in self.jinja2_env.filters:
            raise Exception(u'Filter "%s" already registered ' % filter_name)
        self.jinja2_env.filters[filter_func] = filter_func

    @property
    def sql_log(self):
        raise NotImplementedError

    def get_connection(self, name):
        raise NotImplementedError

    def get_connection_vendor(self):
        raise NotImplementedError

    def add_table(self, name, table):
        if name in self.tables:
            return
        table.name = name
        self.tables[name] = table
        self.tables_list.append(table)
        if table.main:
            self.from_list.append(table)
        else:
            self.join_list.append(table)

    def add_where(self, fltr):
        if fltr in self.where_list:
            return
        self.where_list.append(fltr)

    def get_report_template(self, report_template_id):
        raise NotImplementedError

    def get_config(self, unbound=False):
        group_by_choices = []
        for field_name in self.meta.group_by:
            field = self.fields[field_name]
            group_by_choices.append({
                'id': field.name,
                'name': field.name,
                'label': field.label
            })

        order_by = []
        for item in self.order_by_list:
            order_by.append({
                'direction': item['direction'],
                'field_name': item['field'].name,
                'subtotal': item['subtotal']
            })

        config = {
            'name': self.name,
            'title': self.title,
            'fields': [],
            'filters': [],
            'placeholders': [],
            'having': [],
            'group_by_choices': group_by_choices,
            'group_by': [x.name for x in self.group_by_list],
            'order_by': order_by,
            'show_total': self.show_total,
            'enumerate_rows': self.enumerate_rows,
            'subtotals': [],
            'email': self.email,
            'mime_type': self.mime_type,
            'show_sql': self.show_sql,
            'show_fields': self.show_fields,
            'show_group_by': self.show_group_by,
            'show_order_by': self.show_order_by,
            'template': self.template,
            'template_id': self.report_template.id if self.report_template else None,
            'template_name': self.report_template.name if self.report_template else None,
            'periodicity': self.periodicity,
            'public': self.report_template.public if self.report_template else False,
            'operators': OPERATORS.items(),
            'mime_types': [{'id': k, 'name': RENDERERS[k][0].description} for k in self.mime_types if k in RENDERERS],
            'periodicities': [{'id': k, 'name': v} for k, v in PERIODICITIES.items()],
        }
        # get fields
        fields = sorted(self.fields_list, key=attrgetter('order', 'position'))
        for field in fields:
            if not field.hidden:
                config['fields'].append({
                    'id': field.name,
                    'name': field.name,
                    'label': field.label,
                    'show': field.show
                })

        # get filters
        for fltr in self.filters_list:
            if fltr.hidden:
                continue
            filter_value = []
            if unbound and fltr.default and not fltr.save:
                filter_value = fltr.default() if callable(fltr.default) else fltr.default
            if not filter_value:
                filter_value = fltr.get_config_value()
            config['filters'].append({
                'id': fltr.name,
                'name': fltr.name,
                'label': fltr.label,
                'operator': fltr.operator,
                'value': filter_value,
                'show_values': fltr.show_values,
                'widget': fltr.widget_name,
                'widget_data': fltr.widget_data() if callable(fltr.widget_data) else fltr.widget_data,
                'widget_initial': fltr.widget_initial() if callable(fltr.widget_initial) else fltr.widget_initial,
                'is_free': fltr.is_free,
                'required': fltr.required,
                'advanced': fltr.advanced
            })

        # get placeholders
        for pl in self.placeholders_list:
            config['placeholders'].append({
                'name': pl.name,
                'label': pl.label,
                'value': pl.value
            })
        return config

    def load_config(self, data):
        self.config = data
        self.where_list = []
        self.having_list = []
        self.group_by_list = []
        self.order_by_list = []
        # report settings
        self.title = data.get('title', u'')
        self.show_total = data.get('show_total', False)
        self.enumerate_rows = data.get('enumerate_rows', False)
        self.show_sql = data.get('show_sql', False)
        self.template = data.get('template')

        # шаблон отчета
        template_id = data.get('template_id')
        if template_id:
            self.report_template = self.get_report_template(template_id)
        else:
            self.report_template = None

        if self.report_template:
            self.report_template.name = data.get('template_name', '')

        self.mime_type = data.get('mime_type', 'text/html')
        self.periodicity = data.get('periodicity', None)
        self.email = data.get('email', '')

        # FIELDS
        for num, item in enumerate(data['fields']):
            try:
                f = self.fields[item['name']]
            except KeyError:
                continue
            f.show = item['show']
            f.order = num

        self.visible_fields = sorted(
            filter(lambda x: x.show and not x.hidden, self.fields_list),
            key=attrgetter('order', 'position')
        )
        self.hidden_fields = filter(lambda x: x.hidden, self.fields_list)
        self.real_visible_fields = filter(lambda x: not isinstance(x, VirtualField), self.visible_fields)
        self.virtual_visible_fields = filter(lambda x: isinstance(x, VirtualField), self.visible_fields)

        # FILTERS
        for item in data['filters']:
            name, operator, value = item['name'], item['operator'], item['value']
            try:
                fltr = self.filters[name]
            except KeyError:
                continue
            fltr.value = []
            fltr.verbose_values = []
            fltr.load_config_value(value)
            fltr.show_values = item.get('show_values', False)
            fltr.operator = int(operator)
            if isinstance(fltr, Having):
                self.having_list.append(fltr)
            else:
                self.where_list.append(fltr)
        for item in data.get('having', []):
            name, operator, value = item['name'], item['operator'], item['value']
            fltr = self.filters[name]
            fltr.value = to_python(value, fltr.value_type)
            fltr.operator = operator
            self.having_list.append(fltr)

        # PLACEHOLDERS
        for pl_data in data.get('placeholders', []):
            pl_name = pl_data['name']
            try:
                pl = self.placeholders[pl_name]
            except KeyError:
                continue
            pl.value = pl_data['value']

        # GROUP BY
        for name in data['group_by']:
            try:
                field = self.fields[name]
                assert field.alias in self.meta.group_by
            except (KeyError, AssertionError):
                continue
            self.group_by_list.append(field)

        # ORDER BY
        for item in data.get('order_by', []):
            direction = item.get('direction', 'ASC')
            subtotal = item.get('subtotal', False)
            field_alias = item['field_name']
            try:
                field = self.fields[field_alias]
            except KeyError:
                continue
            self.order_by_list.append({
                'direction': direction,
                'field': field,
                'subtotal': subtotal
            })

    def set_field_visibility(self, field, value):
        try:
            visible_fields_index = self.visible_fields.index(field)
        except ValueError:
            visible_fields_index = None
        try:
            real_fields_index = self.real_visible_fields.index(field)
        except ValueError:
            real_fields_index = None
        try:
            virtual_fields_index = self.virtual_visible_fields.index(field)
        except ValueError:
            virtual_fields_index = None
        try:
            orderby_index = self.order_by_list.index(field)
        except ValueError:
            orderby_index = None

        is_virtual = isinstance(field, VirtualField)

        field.show = value

        if value is False:
            if visible_fields_index is not None:
                self.visible_fields.pop(visible_fields_index)
            if real_fields_index is not None:
                self.real_visible_fields.pop(real_fields_index)
            if virtual_fields_index is not None:
                self.virtual_visible_fields.pop(virtual_fields_index)
            if orderby_index is not None:
                self.order_by_list.pop(orderby_index)
        else:
            if visible_fields_index is None:
                self.visible_fields.append(field)
            if not is_virtual and real_fields_index is None:
                self.real_visible_fields.append(field)
            if is_virtual and virtual_fields_index is None:
                self.virtual_visible_fields.append(field)

    def get_context(self):
        context = {}
        context.update(self.extra_context)
        return context

    def sql(self, format=False, get_params=False):
        #
        # Подготовка данных для генерации / шаблона
        #
        # поля, не выбранные для отображения, но выбранные для группировки
        missing_fields = set(self.group_by_list) - set(self.real_visible_fields)

        fields = []
        if not self.real_visible_fields:
            raise BadParameters(u'No fields is selected')

        self.fields_for_sql = self.real_visible_fields + list(missing_fields) + self.hidden_fields
        # подключаем зависимости
        for f in self.virtual_fields:
            if not f.show:
                continue
            for name in f.depends:
                dependency_f = self.fields[name]
                if dependency_f not in self.fields_for_sql:
                    self.fields_for_sql.append(dependency_f)

        # генерация sql
        if not self.sql_template:
            tmpl = u'\n'.join((
                '%(with_queries)s',
                'SELECT %(distinct)s %(fields)s',
                'FROM %(from)s',
                '%(join)s',
                '%(where)s',
                '%(group_by)s',
                '%(having)s',
                '%(order_by)s'))
            where_list = []
            for x in self.where_list:
                if isinstance(x, PlainFilter):
                    continue
                # skip filter if only_if(value) != True
                if x.only_if is not None and not x.only_if(x.value):
                    continue

                sql = x.get_sql()
                if sql:
                    where_list.append(sql)

            where_sql = ''
            if where_list:
                where_sql = u'WHERE %s' % u' \nAND '.join(where_list)

            having_list = []
            for x in self.having_list:
                sql = x.get_sql()
                if sql:
                    having_list.append(sql)

            having_sql = u' AND \n'.join(having_list)
            # используем default значение, если ничего не указано
            group_by_list = self.group_by_list
            if not group_by_list and self.default_group_by:
                group_by_list = []
                for alias in self.default_group_by:
                    f = self.fields.get(alias)
                    if not f:
                        continue
                    group_by_list.append(f)
                    if f not in self.fields_for_sql:
                        self.fields_for_sql.append(f)
            group_by_sql_list = [u'%s' % x.get_sql(False, vendor=self.vendor, alias=False) for x in group_by_list]

            order_by = []
            for item in self.order_by_list:
                if item['field'].order_by:
                    # check that order sql statement in group by
                    if item['field'].order_by not in group_by_sql_list:
                        group_by_sql_list.append(item['field'].order_by)
                    order_by.append(u'%s %s' % (item['field'].order_by, item['direction']))
                else:
                    order_by.append(u'"%s" %s' % (item['field'].alias, item['direction']))

            group_by_sql = u', '.join(group_by_sql_list)

            for f in self.fields_for_sql:
                aggregate = group_by_list and f not in group_by_list
                field_sql = f.get_sql(aggregate, vendor=self.vendor)
                fields.append(field_sql)
            fields_sql = u', \n'.join(fields)

            # вычисляем необходимые таблицы
            used_optional_tables = []
            for x in self.fields_for_sql + [f for f in self.filters_list if f.value]:
                if x.using_table is not None:
                    if isinstance(x.using_table, basestring):
                        used_optional_tables.append(x.using_table)
                    elif isinstance(x.using_table, (tuple, list)):
                        used_optional_tables.extend(x.using_table)

            join_list = [t for t in self.join_list if not t.optional or t.reference_name in used_optional_tables]

            from_sql = u', \n'.join([x.get_sql() for x in self.from_list])
            join_sql = u'\n'.join([x.get_sql() for x in join_list if x.get_sql() is not None])

            params = {
                'fields': fields_sql,
                'from': from_sql,
                'join': join_sql,
                'where': where_sql,
                'group_by': u'GROUP BY %s' % group_by_sql if group_by_sql else u'',
                'having':  u'HAVING %s' % having_sql if group_by_sql and having_sql else u'',
                'order_by': u'ORDER BY %s' % u', '.join(order_by) if order_by else u'',
                'distinct': 'DISTINCT' if self.distinct else '',
                'with_queries': self.with_queries or '',
            }
            if get_params:
                return params
            sql = tmpl % params
        else:
            sql = sql_format(self.sql_template, self.get_context())

        if format:
            sql = u'\n'.join([line.strip() for line in sql.split('\n')])

        return sql

    @property
    def cursor(self):
        if not self._cursor:
            self._cursor = self.connection.cursor()
            self.register_cursor(self._cursor)
            if self.vendor == 'mysql':
                self.cursor.execute("SET sql_mode = ANSI_QUOTES")
        return self._cursor

    def register_cursor(self, cursor):
        # FIXME: commented due read-only `execute` of cursor
        # old_execute = cursor.cursor.execute

        # def execute_wrapper(*args, **kwargs):
        #     t0 = time.time()
        #     old_execute(*args, **kwargs)
        #     t1 = time.time()
        #     _sql = args[0]
        #     _sql = '\n'.join([x.strip() for x in _sql.split('\n')])
        #     self._sql_log.append({
        #         'time': round((t1-t0) * 1000, 1),
        #         'sql': _sql
        #     })
        # # override low-level execution (for formatted sql)
        # cursor.cursor.execute = execute_wrapper
        pass
    def execute(self):
        if self.vendor == 'postgresql':
            self.cursor.execute('BEGIN')
        # preprocessing
        if hasattr(self, 'preprocess'):
            try:
                self.preprocess()
            except Exception as e:
                msg = e.message or repr(e)
                self.error = msg.decode('utf-8') if not isinstance(msg, unicode) else msg
                return

        sql = self.sql()

        try:
            self.cursor.execute(sql)
        except Exception as e:
            self.error = e.message.decode('utf-8') if not isinstance(e.message, unicode) else e.message
            return
        data = self.cursor.fetchall()
        result = []
        _to_python = to_python

        for num, rec in enumerate(data, 1):
            row = Row()
            row._number = num
            row.REPORT = self
            # real fields
            if not self.sql_template:
                for num, field in enumerate(self.fields_for_sql):
                    # явное преобразование типов
                    if isinstance(rec[num], basestring) and field.type != STR:
                        cur_value = _to_python(rec[num], field.type)
                        row[field.name] = cur_value
                        continue
                    elif isinstance(rec[num], datetime.datetime):
                        cur_value = rec[num]
                        # timezone handling
                        if cur_value.tzinfo is not None:
                            cur_value = cur_value.astimezone(get_tz())
                        row[field.name] = cur_value
                        continue
                    setattr(row, field.name, rec[num])

            else:
                for num, col in enumerate(self.cursor.description):
                    try:
                        field = self.fields[col[0]]
                    except:
                        field = None
                    # явное преобразование типов
                    if field and isinstance(rec[num], basestring) and field.type != STR:
                        cur_value = _to_python(rec[num], field.type)
                        row[col[0]] = cur_value
                        continue
                    elif isinstance(rec[num], datetime.datetime):
                        cur_value = rec[num]
                        # timezone handling
                        if cur_value.tzinfo is not None:
                            cur_value = cur_value.astimezone(get_tz())
                        row[col[0]] = cur_value
                        continue
                    setattr(row, col[0], rec[num])
            # virtual fields
            for field in self.virtual_visible_fields:
                setattr(row, field.name, field.func(row))
            result.append(row)

        # postprocessing
        if hasattr(self, 'postprocess'):
            try:
                result = self.postprocess(result)
            except Exception as e:
                self.error = e.message.decode('utf-8') if not isinstance(e.message, unicode) else e.message
                return

        if self.vendor == 'postgresql':
            self.cursor.execute('ROLLBACK')

        return result

    def calculate_total(self, data):
        total = Row()

        total_real_fields = filter(
            lambda x: x.calculate_total and not isinstance(x, VirtualField),
            self.fields_for_sql
        )
        total_virtual_fields = filter(
            lambda x: x.calculate_total and isinstance(x, VirtualField),
            self.visible_fields
        )
        # subtotal fields
        total.subtotals = []
        subtotal_fields = [x['field'].name for x in self.order_by_list if x['subtotal']]
        # speed optimization
        if subtotal_fields:
            for k, g in groupby(data, attrgetter(*subtotal_fields)):
                if not isinstance(k, tuple):
                    k = (k,)
                group = list(g)
                subtotal = Row()
                # init values
                for f in self.fields_for_sql:
                    if f.type == INTERVAL:
                        init_value = datetime.timedelta()
                    else:
                        init_value = 0
                    setattr(subtotal, f.name, init_value)

                for row in group:
                    for f in total_real_fields:
                        cur_value = row[f.name]
                        prev_value = getattr(subtotal, f.name)
                        if cur_value is not None:
                            setattr(subtotal, f.name, f.total_func(prev_value, cur_value))

                # calc virtualfield
                for f in total_virtual_fields:
                    setattr(subtotal, f.name, f.func(subtotal))
                total.subtotals.append({
                    'key_values': dict(zip(subtotal_fields, k)),
                    'group_keys': tuple(subtotal_fields),
                    'group': group,
                    'total': subtotal
                })

        # init values
        for f in self.fields_for_sql:
            if f.type == INTERVAL:
                init_value = datetime.timedelta()
            else:
                init_value = 0
            setattr(total, f.name, init_value)

        for row in data:
            for f in total_real_fields:
                prev_value = getattr(total, f.name)
                cur_value = getattr(row, f.name)
                if cur_value is not None:
                    setattr(total, f.name, f.total_func(prev_value, cur_value))
        # calc virtualfield
        for f in total_virtual_fields:
            setattr(total, f.name, f.func(total))
        return total

    def render(self, data=None):
        if data is None:
            data = self.execute()
        total = None

        # enumerate data
        if isinstance(data, list):
            # check attribute "_number"
            if data and isinstance(data[0], dict):
                if not '_number'  in data[0]:
                    for num, rec in enumerate(data, 1):
                        rec['_number'] = num
                # check that data items is Row object
                if not isinstance(data[0], Row):
                    data = [Row(x) for x in data]

        if not self.error and self.show_total:
            total = self.calculate_total(data)

        context = {
            'report': self,
            'data': data,
            'total': total,
            'chunks': chunks,
        }

        try:
            renderer = RENDERERS.get_renderer(self.mime_type)
            file_extension = RENDERERS.get_file_extension(self.mime_type)
        except KeyError:
            raise Exception(u'Unknown MIME-type: %s' % self.mime_type)

        rendered = renderer(self).render(context)
        # after render hook
        if hasattr(self, 'after_render'):
            self.after_render(mimetype=self.mime_type, body=rendered, extension=file_extension)
        return self.mime_type, rendered
