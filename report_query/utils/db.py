# -*- coding: utf-8 -*-
from psycopg2.extensions import adapt

def to_unicode(s, charset='utf-8'):
    return s if isinstance(s, unicode) else s.decode(charset)

def fetchone_dict(cursor):
    desc = cursor.description
    row = cursor.fetchone()
    if not row:
        return {}
    return dict(zip([col[0] for col in desc], row))

def fetchall_dict(cursor):
    desc = cursor.description
    return [dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()]

def quote(obj):
    if isinstance(obj, (list, tuple)):
        return '(%s)' % ', '.join(map(quote, obj))
    elif isinstance(obj, unicode):
        adapted = adapt(obj.encode('utf-8'))
    else:
        adapted = adapt(obj)
    return to_unicode(adapted.getquoted())


def quote_dict(d):
    result = {}
    for k, v in d.iteritems():
        result[k] = quote(v)
    return result


def sql_format(*args):
    args = list(args)
    tmpl = args.pop(0)
    # if dict
    if len(args) == 1 and isinstance(args[0], dict):
        return tmpl % quote_dict(args[0])

    params = tuple(map(quote, args))
    return tmpl % params


class SqlTemplate:

    def __init__(self, tmpl):
        self.tmpl = tmpl

    def __mod__(self, data):
        if isinstance(data, dict):
            return sql_format(self.tmpl, data)
        elif isinstance(data, (list, tuple)):
            return sql_format(self.tmpl, *data)
        else:
            return sql_format(self.tmpl, data)


def list_to_pg_literal_array(l):
    _l = filter(lambda x: x is not None, l)
    def convert(value):
        if isinstance(value, basestring):
            return '"%s"' % value
        elif isinstance(value, (int, long)):
            return '%d' % value
        elif isinstance(value, float):
            return '%.3f' % value

    return '{%s}' % ','.join(map(convert, _l))

def get_chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

if __name__ == '__main__':
    t = (1, ('2', '3'), 3.5)
    d = {'first': 1, 'second': ['1'], 'third': 3}

    tmpl_for_list = "foo = %s AND bar IN %s AND baz = %s"
    tmpl_for_dict = "foo = %(first)s AND bar IN %(second)s AND baz = %(third)s"

    result_for_list = "foo = 1 AND bar IN ('2', '3') AND baz = 3.5"
    result_for_dict = "foo = 1 AND bar IN ('1') AND baz = 3"

    assert sql_format(tmpl_for_list, *t) == result_for_list
    assert sql_format(tmpl_for_dict, d) == result_for_dict

    print 'Tuple:', t, 'Dict:', d
    print sql_format(tmpl_for_list, *t)
    print sql_format(tmpl_for_dict, d)

    tmpl_for_list = SqlTemplate(tmpl_for_list)
    tmpl_for_dict = SqlTemplate(tmpl_for_dict)
    assert (tmpl_for_list % t) == result_for_list
    assert (tmpl_for_dict % d) == result_for_dict
    print tmpl_for_list % t
    print tmpl_for_dict % d
