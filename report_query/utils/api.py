import datetime
import json
from .timezone import to_local_tz


class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            if obj.tzinfo is None:
                _obj = to_local_tz(obj)
            else:
                _obj = obj
            _obj = _obj.replace(microsecond=0)
            return _obj.isoformat(' ')
        elif isinstance(obj, datetime.date):
            return obj.isoformat()
        elif isinstance(obj, datetime.timedelta):
            total_seconds = obj.total_seconds()
            hours, remainder = divmod(abs(total_seconds), 3600)
            minutes, seconds = divmod(remainder, 60)
            return '%s%02d:%02d:%02d' % (('', '-')[total_seconds < 0], hours, minutes, seconds)
        return super(CustomEncoder, self).default(obj)
