from __future__ import absolute_import
import os
import datetime

import pytz
from dateutil.parser import parse
from dateutil import tz

def set_tz(timezone):
    os.environ['TIMEZONE_NAME'] = timezone

def datetime_to_utc(value):
    if value.tzinfo:
        value = pytz.utc.normalize(value)
    else:
        value = pytz.utc.localize(value)
    return value

def to_local_tz(value):
    local_tz = get_tz()
    if value.tzinfo:
        if isinstance(value.tzinfo, (tz.tzutc, tz.tzoffset)):
            value = value.astimezone(local_tz)
        else:
            value = local_tz.normalize(value)
    else:
        value = local_tz.localize(value)
    return value

def now_local():
    local_tz = get_tz()
    return local_tz.localize(datetime.datetime.now())

def get_tz():
    TIMEZONE = os.environ['TIMEZONE_NAME']
    return pytz.timezone(TIMEZONE)
