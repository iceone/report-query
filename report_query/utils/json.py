# -*- coding: utf-8 -*-
"""
    report_query.utils.json
    ~~~~~~~~~~~~~~~~~~~

    JSON serialization utilities.

    :copyright: (c) 2015 by Ilnur Ibragimov
"""
from __future__ import absolute_import, unicode_literals, print_function, division
import datetime
from flask.json import JSONEncoder as FlaskJSONEncoder
from psycopg2.extras import DateTimeTZRange


class JSONEncoder(FlaskJSONEncoder):

    def default(self, o):
        if isinstance(o, datetime.datetime):
            # TODO: remove this shit
            if o.year < 1900:
                return o.replace(year=1900).strftime('%Y-%m-%dT%H:%M:%S%z')
            return o.strftime('%Y-%m-%dT%H:%M:%S%z')
        elif isinstance(o, datetime.date):
            return o.strftime('%Y-%m-%d')
        elif isinstance(o, datetime.timedelta):
            total_seconds = o.total_seconds()
            hours, remainder = divmod(abs(total_seconds), 3600)
            minutes, seconds = divmod(remainder, 60)
            return '%s%02d:%02d:%02d' % \
                (('', '-')[total_seconds < 0], hours, minutes, seconds)
        elif isinstance(o, DateTimeTZRange):
            return [o.lower, o.upper]
        return super(JSONEncoder, self).default(o)
