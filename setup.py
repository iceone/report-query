import os
import re
import ast
from setuptools import setup


_version_re = re.compile(r'__VERSION__\s+=\s+(.*)')

MODULE_PATH = os.path.join(
    os.path.dirname(
        os.path.abspath(__file__)),
    'report_query/__init__.py')

with open(MODULE_PATH, 'rb') as f:
    version = str(ast.literal_eval(_version_re.search(
        f.read().decode('utf-8')).group(1)))

setup(
    name='report_query',
    author='',
    author_email='',
    version=version,
    url='http://bitbucket.org/iceone/report-query',
    packages=['report_query'],
    install_requires=[
        'Jinja2==2.7.2',
        'psycopg2==2.5.3',
        'py==1.4.20',
        'pytest==2.5.2',
        'python-dateutil==2.2',
        'pytz',
        'six==1.6.1',
        'sqlautocode==0.7',
        'tox==1.7.1',
        'virtualenv==1.11.6',
        'pygments',
        'blinker'
    ],
    description='',
    classifiers=[
    ],
)
